#include "LPC8xx.h"
#include "score.h"

NoteLength note_lengths[6][2] = {
	/*  play time   ,   rest time   */
	{_8TH_LEGATO,    _8TH_LEGATO_REST},
	{_8TH,           _8TH_REST},
	{_8TH_STACCATO,  _8TH_STACCATO_REST},
	{_16TH_LEGATO,   _16TH_LEGATO_REST},
	{_16TH,          _16TH_REST},
	{_16TH_STACCATO, _16TH_STACCATO_REST},
};

// note frequences C0..B7
uint32_t note_freqs[84];

// 5-limit tuning with tonic C
static uint8_t intervals[12][2] = {
	{1,  1},
	{16, 15},
	{9,  8},
	{6,  5},
	{5,  4},
	{4,  3},
	{45, 32},
	{3,  2},
	{8,  5},
	{5,  3},
	{16, 9},
	{15, 8},
};

void compute_note_freqs(void)
{
	int octave, note;

	for (octave = 0; octave < 8; octave++) {
		for (note = 0; note < 12; note++) {
			uint32_t freq;
			freq = (BASE_FREQ << octave) *
				intervals[note][0] /
				intervals[note][1];
			note_freqs[octave * 12 + note] = freq;
		}
	}
}

/*
 * П. И. Чайковский: Танец феи Драже.
 */
NoteEvent track0[] = {
	// 32
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 7),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 6),
	},
	// 33
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 3),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 2),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 2),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 2),
	},
	// 34
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 1),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 1),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 1),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 0),
	},
	// 35
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = endOfTrack,
		.pitch  = REST,
	}
};

NoteEvent track1[] = {
	// 32
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 7),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 2),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	// 33
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 0),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 1),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 2),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 2),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 2),
	},
	// 34
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 1),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 1),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 1),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 0),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 0),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 0),
	},
	// 35
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 2),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 2),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 0),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 2),
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = endOfTrack,
		.pitch  = REST,
	}
};

	/*
	{
		.length = _8TH_STACCATO,
		.pitch = PITCH_E3,
	},
	{
		.length = _8TH,
		.pitch = REST,
	},
	*/

//extern typeof(track1) track1;
//extern typeof(track3) track3;
