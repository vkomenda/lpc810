/***********************************************************************
 *
 *   LPC810 plays a tune using PWM.
 *   Copyright (c) 2014, Vladimir Komendantskiy
 *   MIT License
 *
 **********************************************************************/
#include <stdio.h>
#include "LPC8xx.h"
#include "gpio.h"
#include "mrt.h"
#include "uart.h"
#include "notes.h"

#define SOUNDER_PIN (2)
#define PIN_HIGH(p) (LPC_GPIO_PORT->SET0 = 1 << p)
#define PIN_LOW(p)  (LPC_GPIO_PORT->CLR0 = 1 << p)

//#define USE_SWD

//#define NT(freq, nomer, denum) NOTE(freq, nomer, denum, TEMPO_ANDANTE)

note tune[5] = {
	NOTE(FREQ_C3,  1, 8, TEMPO_ANDANTE),
	NOTE(FREQ_G3,  1, 8, TEMPO_ANDANTE),
	NOTE(FREQ_D4,  1, 8, TEMPO_ANDANTE),
	NOTE(FREQ_Eb4, 1, 8, TEMPO_ANDANTE),
	NOTE(FREQ_Bb4, 1, 2, TEMPO_ANDANTE),
};

void configurePins()
{
	/* Enable SWM clock */
	//  LPC_SYSCON->SYSAHBCLKCTRL |= (1 << 7);  // this is already done in SystemInit()

	/* Pin Assign 8 bit Configuration */
	/* U0_TXD */
	/* U0_RXD */
	LPC_SWM->PINASSIGN0 = 0xffff0004UL;

	/* Pin Assign 1 bit Configuration */
#if !defined(USE_SWD)
	/* Pin setup generated via Switch Matrix Tool
	   ------------------------------------------------
	   PIO0_5 = RESET
	   PIO0_4 = U0_TXD
	   PIO0_3 = GPIO            - Disables SWDCLK
	   PIO0_2 = GPIO (User LED) - Disables SWDIO
	   PIO0_1 = GPIO
	   PIO0_0 = U0_RXD
	   ------------------------------------------------
	   NOTE: SWD is disabled to free GPIO pins!
	   ------------------------------------------------ */
	LPC_SWM->PINENABLE0 = 0xffffffbfUL;
#else
	/* Pin setup generated via Switch Matrix Tool
	   ------------------------------------------------
	   PIO0_5 = RESET
	   PIO0_4 = U0_TXD
	   PIO0_3 = SWDCLK
	   PIO0_2 = SWDIO
	   PIO0_1 = GPIO
	   PIO0_0 = U0_RXD
	   ------------------------------------------------
	   NOTE: LED on PIO0_2 unavailable due to SWDIO!
	   ------------------------------------------------ */
	LPC_SWM->PINENABLE0 = 0xffffffb3UL;
#endif
}

int main(void)
{
	int i = 0;

	/* Initialise the GPIO block */
	gpioInit();

	/* Initialise the UART0 block for printf output */
	uart0Init(115200);

//	puts("Playing a tune...\n\r");

	/* Configure the multi-rate timer for 1us ticks */
	mrtInit(__SYSTEM_CLOCK/1000000);

	/* Configure the switch matrix (setup pins for UART0 and GPIO) */
	configurePins();

	/* Set the sounder pin to output (1 = output, 0 = input) */
	LPC_GPIO_PORT->DIR0 |= (1 << SOUNDER_PIN);

	while (1) {
		note n = tune[i];
		unsigned int t;
		char msg[15];

//		printf(msg, "%d\n\r", i);

		// play a note
		for (t = 0; t < n.dur; t += n.on + n.off) {
			if (n.on) {
				PIN_HIGH(SOUNDER_PIN);
				mrtDelay(n.on);
			}
			PIN_LOW(SOUNDER_PIN);
			mrtDelay(n.off);
		}

		if (i < sizeof(tune) / sizeof(note))
			// next note
			i++;
		else
			// first note
			i = 0;
	}
}
