//**************************************************************************/
//    Analog Modeling Synth main code
//**************************************************************************/
#include "LPC8xx.h"
#include "mrt.h"
#include "gpio.h"
#include "score.h"

#define SOUNDER_PIN 2
#define LED_PIN     3

#define INPUT  0
#define OUTPUT 1
#define PIN_HIGH(p) (LPC_GPIO_PORT->SET0 = 1 << p)
#define PIN_LOW(p)  (LPC_GPIO_PORT->CLR0 = 1 << p)

uint8_t led_on = 0;

#define SAW    0
#define SQUARE 1
#define SINE   2

//-------- Synth parameters --------------
uint8_t  WAVEFORM  = SAW;
uint32_t FREQ1     = 0x10000; //0x10000=C4
uint32_t FREQ2     = 0x14000; //0x10100; //0x10000=C4
uint32_t FREQ3     = 0x18000; //0x0FF00; //0x10000=C4
int32_t  CUTOFF    = 65535;   //freq 0-65535
int32_t  RESONANCE = 0;       //reso 0-65535
uint8_t  TRIG      = 1;       //MIDItrig 1=note ON
uint32_t ATTACK    = 100;     //Attackrate in 30mS steps
uint32_t DECAY     = 100;     //Decayrate in 30mS steps
int32_t  SUSTAIN   = 0xFFFF;  //Sustainlevel 0-65535
uint32_t RELEASE   = 1;       //Releaserate in 30mS steps
//-----------------------------------------

int32_t err = 0;
volatile uint16_t DAC = 0;

uint32_t DCO1, DCO2, DCO3, ENV;
int32_t t1, t2, t3, t4, t5, t6, t7, t8;
int32_t vi, vo;
int16_t DCO, DCF, LFO, lfocounter = 1;
uint8_t phase;

uint8_t state=0;    //needs to be reset on a new trig
uint16_t envstat=1; //needs to be reset on a new trig
int32_t volume=0;

const int16_t sine[256] = {
	0x0000, 0x0324, 0x0647, 0x096a, 0x0c8b, 0x0fab, 0x12c8, 0x15e2,
	0x18f8, 0x1c0b, 0x1f19, 0x2223, 0x2528, 0x2826, 0x2b1f, 0x2e11,
	0x30fb, 0x33de, 0x36ba, 0x398c, 0x3c56, 0x3f17, 0x41ce, 0x447a,
	0x471c, 0x49b4, 0x4c3f, 0x4ebf, 0x5133, 0x539b, 0x55f5, 0x5842,
	0x5a82, 0x5cb4, 0x5ed7, 0x60ec, 0x62f2, 0x64e8, 0x66cf, 0x68a6,
	0x6a6d, 0x6c24, 0x6dca, 0x6f5f, 0x70e2, 0x7255, 0x73b5, 0x7504,
	0x7641, 0x776c, 0x7884, 0x798a, 0x7a7d, 0x7b5d, 0x7c29, 0x7ce3,
	0x7d8a, 0x7e1d, 0x7e9d, 0x7f09, 0x7f62, 0x7fa7, 0x7fd8, 0x7ff6,
	0x7fff, 0x7ff6, 0x7fd8, 0x7fa7, 0x7f62, 0x7f09, 0x7e9d, 0x7e1d,
	0x7d8a, 0x7ce3, 0x7c29, 0x7b5d, 0x7a7d, 0x798a, 0x7884, 0x776c,
	0x7641, 0x7504, 0x73b5, 0x7255, 0x70e2, 0x6f5f, 0x6dca, 0x6c24,
	0x6a6d, 0x68a6, 0x66cf, 0x64e8, 0x62f2, 0x60ec, 0x5ed7, 0x5cb4,
	0x5a82, 0x5842, 0x55f5, 0x539b, 0x5133, 0x4ebf, 0x4c3f, 0x49b4,
	0x471c, 0x447a, 0x41ce, 0x3f17, 0x3c56, 0x398c, 0x36ba, 0x33de,
	0x30fb, 0x2e11, 0x2b1f, 0x2826, 0x2528, 0x2223, 0x1f19, 0x1c0b,
	0x18f8, 0x15e2, 0x12c8, 0x0fab, 0x0c8b, 0x096a, 0x0647, 0x0324,
	0x0000, 0xfcdc, 0xf9b9, 0xf696, 0xf375, 0xf055, 0xed38, 0xea1e,
	0xe708, 0xe3f5, 0xe0e7, 0xdddd, 0xdad8, 0xd7da, 0xd4e1, 0xd1ef,
	0xcf05, 0xcc22, 0xc946, 0xc674, 0xc3aa, 0xc0e9, 0xbe32, 0xbb86,
	0xb8e4, 0xb64c, 0xb3c1, 0xb141, 0xaecd, 0xac65, 0xaa0b, 0xa7be,
	0xa57e, 0xa34c, 0xa129, 0x9f14, 0x9d0e, 0x9b18, 0x9931, 0x975a,
	0x9593, 0x93dc, 0x9236, 0x90a1, 0x8f1e, 0x8dab, 0x8c4b, 0x8afc,
	0x89bf, 0x8894, 0x877c, 0x8676, 0x8583, 0x84a3, 0x83d7, 0x831d,
	0x8276, 0x81e3, 0x8163, 0x80f7, 0x809e, 0x8059, 0x8028, 0x800a,
	0x8000, 0x800a, 0x8028, 0x8059, 0x809e, 0x80f7, 0x8163, 0x81e3,
	0x8276, 0x831d, 0x83d7, 0x84a3, 0x8583, 0x8676, 0x877c, 0x8894,
	0x89bf, 0x8afc, 0x8c4b, 0x8dab, 0x8f1e, 0x90a1, 0x9236, 0x93dc,
	0x9593, 0x975a, 0x9931, 0x9b18, 0x9d0e, 0x9f14, 0xa129, 0xa34c,
	0xa57e, 0xa7be, 0xaa0b, 0xac65, 0xaecd, 0xb141, 0xb3c1, 0xb64c,
	0xb8e4, 0xbb86, 0xbe32, 0xc0e9, 0xc3aa, 0xc674, 0xc946, 0xcc22,
	0xcf05, 0xd1ef, 0xd4e1, 0xd7da, 0xdad8, 0xdddd, 0xe0e7, 0xe3f5,
	0xe708, 0xea1e, 0xed38, 0xf055, 0xf375, 0xf696, 0xf9b9, 0xfcdc
};

void synthStep(void)
{
	gpioSetDir(SOUNDER_PIN, INPUT);

	// 3 DCO block
	DCO1 += FREQ1; //increment phase accumulator1 with phase1
	DCO2 += FREQ2; //increment phase accumulator2 with phase2
	DCO3 += FREQ3; //increment phase accumulator3 with phase3
	switch (WAVEFORM) {
	case SAW:
		DCO = ((((DCO1 & 0x7FFFFF) +
			 (DCO2 & 0x7FFFFF) +
			 (DCO3 & 0x7FFFFF)
			) / 65536) - 192) * 171;
		break;
	case SQUARE:
		DCO = ((((DCO1 & 0x400000) +
			 (DCO2 & 0x400000) +
			 (DCO3 & 0x400000)) /
			65536) - 96) * 341;
		break;
	case SINE:
		DCO = (sine[(DCO1>>15) & 255] / 4) +
		      (sine[(DCO2>>15) & 255] / 4) +
		      (sine[(DCO3>>15) & 255] / 4);
		break;
	}
	//------------

	// DCF block
	// C implementation for a 4-pole lowpass DCF with resonance:
	vi = DCO;
	vo = ((vo * (65536 - CUTOFF)) + (t1 * CUTOFF)) / 65536; //+3db
	t1 = ((t1 * (65536 - CUTOFF)) + (t2 * CUTOFF)) / 65536; //+6db
	t2 = ((t2 * (65536 - CUTOFF)) + (t3 * CUTOFF)) / 65536; //+9db
	t3 = ((t3 * (65536 - CUTOFF)) + (t4 * CUTOFF)) / 65536; //+12db
	t4 = ((t4 * (65536 - CUTOFF)) + (t5 * CUTOFF)) / 65536; //+15db
	t5 = ((t5 * (65536 - CUTOFF)) + (t6 * CUTOFF)) / 65536; //+18db
	t6 = ((t6 * (65536 - CUTOFF)) + (t7 * CUTOFF)) / 65536; //+21db
	t7 = ((t7 * (65536 - CUTOFF)) + (t8 * CUTOFF)) / 65536; //+24db
	t8 = vi - ((vo * RESONANCE) / 65536);                 //resonance feedback
	DCF = vo;
	//---------

	// ENV block
	if (!envstat--) {
		envstat = 1024;
		if (TRIG > 0) {
			if (state == 0) {
				volume += ATTACK;
				if (volume > 0xFFFF) {
					volume = 0xFFFF;
					state++;
				}
			}
			if (state == 1) {
				volume -= DECAY;
				if (volume < SUSTAIN) {
					volume = SUSTAIN;
					state++;
				}
			}
			else {
				volume -= RELEASE;
				if (volume < 0) {
					volume = 0;
				}
			}
		}
	}
	ENV = (volume * DCF) >> 16;
	//----------

	DAC = 0x8000 + ENV;

	gpioSetDir(SOUNDER_PIN, OUTPUT);
}

/* note event indices in the score for 2 separate tracks */
uint32_t trackpos[2] = {0, 0};

uint32_t trackplay[2] = {0, 0};
uint32_t trackrest[2] = {0, 0};

//extern NoteEvent track0[];
//extern NoteEvent track1[];
//NoteEvent* track[2] = {track0, track1};

NoteEvent dummy_track[] = {
	{
		.length = 255,
		.pitch  = NOTE(4, 0),
	},
	{
		.length = endOfTrack,
		.pitch  = REST,
	}
};

void scoreStep(void)
{
	NoteEvent event;
	uint8_t i = 0;

	return;

//	for (i = 0; i < 2; i++) {
		if (!trackplay[i] && !trackrest[i]) {
			event = dummy_track[trackpos[i]];
			if (event.length == endOfTrack) {
				/* replay the track */
				trackpos[i] = 0;
				event = dummy_track[0];
			}

			/* fetch the next event */
			trackplay[i] = 60 * note_lengths[event.length][0];
			trackrest[i] = 60 * note_lengths[event.length][1];
			/* update the index to the next position */
			trackpos[i] += 1;
		}
		if (trackplay[i]) {
			/* play the note: set the channel frequency */
			FREQ1 = FREQ2 = FREQ3 = note_freqs[event.pitch];
			/*
			switch (i) {
			case 0:
				FREQ1 = note_freqs[event.pitch];
				FREQ2 = FREQ1;   // augement track 1
				break;
			case 1:
			default:
				FREQ2 = note_freqs[event.pitch];
				break;
			}
			*/
			trackplay[i] -= 1;
		}
		else if (trackrest[i]) {
			/* play the rest */
			FREQ1 = FREQ2 = FREQ3 = 0;
			/*
			switch (i) {
			case 0:
				FREQ1 = FREQ2 = 0;
				break;
			case 1:
			default:
				FREQ3 = 0;
			}
			*/
			trackrest[i] -= 1;
		}
//	}
}

void MRT_IRQHandler(void)
{
	uint8_t led_on = 0;

	if (LPC_MRT->Channel[0].STAT & MRT_STAT_IRQ_FLAG) {
		/* clear interrupt flag */
		LPC_MRT->Channel[0].STAT = MRT_STAT_IRQ_FLAG;
		synthStep();
	}

	if (LPC_MRT->Channel[1].STAT & MRT_STAT_IRQ_FLAG) {
		/* clear interrupt flag */
		LPC_MRT->Channel[1].STAT = MRT_STAT_IRQ_FLAG;
		scoreStep();
	}
}

void main(void)
{
	uint32_t delays[4] =
		{
			__SYSTEM_CLOCK / 33488,
			0, // __SYSTEM_CLOCK / WHOLE,
			0,
			0
		};

	/* Initialise the GPIO block */
	gpioInit();

	/* Enable all 6 GPIO. */
	LPC_SWM->PINENABLE0 = 0xffffffffUL;

	/* Configure the multi-rate timer for synth and note ticks */
	mrtInit(delays);

	gpioSetDir(SOUNDER_PIN, OUTPUT);
	gpioSetDir(LED_PIN, OUTPUT);

	/* Initialise note frequences in the memory */
	compute_note_freqs();

	while (1) {
		err = (uint16_t) err - (1 << 15) - DAC;
		// if DAC > err, set pin 2, otherwise clear pin 2
		LPC_GPIO_PORT->W0[SOUNDER_PIN] = err >> 16;

		/* variant 1:
		if(DAC >= err) {
			PIN_HIGH(SOUNDER_PIN);
			err += 0xFFFF - DAC;
		}
		else {
			PIN_LOW(SOUNDER_PIN);
			err -= DAC;
		}
		*/

		/* variant 2:
		asm volatile(
			"        mov	r1,#0xA0   \n"
			"        lsl    r1,#24     \n"  // r1=0xA000
			"        mov	r2,#0x10   \n"
			"        lsl    r2,#8      \n"  // pin 2 word reg offset
			"        add	r2,#0x08   \n"  // r2=0x1008
			"        add    r1,r2      \n"  // r1=0xA0001008, pin 2
			"        mov	r6,#0xFF   \n"
			"        lsl    r6,#8      \n"
			"        add	r6,#0xFF   \n"  // r6=0x0000FFFF
			"        mov	r5,#0x01   \n"
			"        lsl    r5,#16     \n"  // r5=0x00010000
			"        mov    r2,#0      \n"  // r2 <- DAC 1-bit error
			"loop%=: ldrh   r7,%0      \n"  // load the amplitude
			"        add	r2,r7      \n"  // r2=phacc
			"        mov	r4,r2      \n"
			"        and	r4,r5      \n"
			"        str	r4,[r1]    \n"  // pin 2
			"        and	r2,r6      \n"
			"        mov    r4,#0      \n"
			"        str	r4,[r1]    \n"  // pin 2
			"        b	loop%=     \n"
			: [dac] "=m" (DAC)
			);
		*/
	}
}
