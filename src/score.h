#ifndef __SCORE_H
#define __SCORE_H

#define BASE_C // tuning base

// note lengths
//#define WHOLE               192    // 64th triplet max. resolution
#define WHOLE               64    // adagio, 64th max. resolution
#define _8TH_LEGATO         (WHOLE / 8)
#define _8TH_LEGATO_REST    0
#define _8TH                (WHOLE / 8 - WHOLE / 8 / 8)
#define _8TH_REST           (_8TH_LEGATO - _8TH)
#define _8TH_STACCATO       (WHOLE / 8 - WHOLE / 8 / 4)
#define _8TH_STACCATO_REST  (_8TH_LEGATO - _8TH_STACCATO)
#define _16TH_LEGATO        (WHOLE / 16)
#define _16TH_LEGATO_REST   0
#define _16TH               (WHOLE / 16 - WHOLE / 16 / 8)
#define _16TH_REST          (_16TH_LEGATO - _16TH_STACCATO)
#define _16TH_STACCATO      (WHOLE / 16 - WHOLE / 16 / 4)
#define _16TH_STACCATO_REST (_16TH_LEGATO - _16TH_STACCATO)

typedef enum NoteLength {
	_8th_legato,
	_8th,
	_8th_staccato,
	_16th_legato,
	_16th,
	_16th_staccato,
	endOfTrack,
} NoteLength;

NoteLength note_lengths[6][2];

// frequences in 5-limit tuning with tonic C
#define _C2   0x4000
#define _Cs2  0x4444
#define _D2   0x4800
#define _Ds2  0x4ccc
#define _E2   0x5000
#define _F2   0x5555
#define _Fs2  0x5a00
#define _G2   0x6000
#define _Gs2  0x6666
#define _A2   0x6aaa
#define _As2  0x71c7
#define _B2   0x7800
#define _C3   0x8000
#define _Cs3  0x8888
#define _D3   0x9000
#define _Ds3  0x9999
#define _E3   0xa000
#define _F3   0xaaaa
#define _Fs3  0xb400
#define _G3   0xc000
#define _Gs3  0xcccc
#define _A3   0xd555
#define _As3  0xe38e
#define _B3   0xf000
#define _C4   0x10000
#define _Cs4  0x11111
#define _D4   0x12000
#define _Ds4  0x13333
#define _E4   0x14000
#define _F4   0x15555
#define _Fs4  0x16800
#define _G4   0x18000
#define _Gs4  0x19999
#define _A4   0x1aaaa
#define _As4  0x1c71c
#define _B4   0x1e000
#define _C5   0x20000
#define _Cs5  0x11111
#define _D5   0x12000
#define _Ds5  0x13333
#define _E5   0x14000
#define _F5   0x15555
#define _Fs5  0x16800
#define _G5   0x18000
#define _Gs5  0x19999
#define _A5   0x1aaaa
#define _As5  0x1c71c
#define _B5   0x1e000
#define _C6   0x20000

#define REST  0xff

typedef struct NoteEvent {
	uint8_t length; // note length relative to the measure length
	uint8_t pitch;  // note pitch 0.. were 0xff denotes a rest
} NoteEvent;

#if defined (BASE_C)

#define BASE_FREQ 0x1000   // frequency of C0
#define NOTE(octave, note) (12 * octave + note) // index in note_freqs[]

#elif defined (BASE_G)

#define BASE_FREQ 0x1800   // frequency of G0
#define NOTE(octave, note) (12 * octave + note - 7) // index in note_freqs[]

#endif // BASE_C

uint32_t note_freqs[84];
void compute_note_freqs(void);

//NoteEvent treble[];
//NoteEvent bass[];

#endif // __SCORE_H
