/*
 * Copyright (c) 2014 Vladimir Komendantskiy
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LPC8xx.h"
#include "mrt.h"
#include "gpio.h"
#include "score.h"

void PMU_DeepPowerDown(void);

#define SOUNDER_PIN 2
#define LED_PIN     3

#define INPUT  0
#define OUTPUT 1

#define OCTAVES 5

// note frequences
static uint16_t note_freqs[OCTAVES * 12];

// 5-limit tuning
static uint8_t intervals[12][2] = {
	{1,  1},
	{16, 15},
	{9,  8},
	{6,  5},
	{5,  4},
	{4,  3},
	{45, 32},
	{3,  2},
	{8,  5},
	{5,  3},
	{16, 9},
	{15, 8},
};

void compute_note_freqs(void)
{
	int octave, note;

	for (octave = 0; octave < OCTAVES; octave++) {
		for (note = 0; note < 12; note++) {
			uint32_t freq;
			freq = (BASE_FREQ << octave) * // tonic
				0x10 *                 // 1 digit after hex "."
				intervals[note][0] /   // numerator
				intervals[note][1];    // denominator
			if (freq & 0x8)
				// fractional part is greater than or equal to 1/2
				freq = (freq >> 4) + 1;
			else
				// fractional part is less than 1/2
				freq = freq >> 4;

			note_freqs[octave * 12 + note] = (uint16_t) freq;
		}
	}
}

static uint8_t wave_divider = 1;

const int16_t sine[256] = {
	0x0000, 0x0324, 0x0647, 0x096a, 0x0c8b, 0x0fab, 0x12c8, 0x15e2,
	0x18f8, 0x1c0b, 0x1f19, 0x2223, 0x2528, 0x2826, 0x2b1f, 0x2e11,
	0x30fb, 0x33de, 0x36ba, 0x398c, 0x3c56, 0x3f17, 0x41ce, 0x447a,
	0x471c, 0x49b4, 0x4c3f, 0x4ebf, 0x5133, 0x539b, 0x55f5, 0x5842,
	0x5a82, 0x5cb4, 0x5ed7, 0x60ec, 0x62f2, 0x64e8, 0x66cf, 0x68a6,
	0x6a6d, 0x6c24, 0x6dca, 0x6f5f, 0x70e2, 0x7255, 0x73b5, 0x7504,
	0x7641, 0x776c, 0x7884, 0x798a, 0x7a7d, 0x7b5d, 0x7c29, 0x7ce3,
	0x7d8a, 0x7e1d, 0x7e9d, 0x7f09, 0x7f62, 0x7fa7, 0x7fd8, 0x7ff6,
	0x7fff, 0x7ff6, 0x7fd8, 0x7fa7, 0x7f62, 0x7f09, 0x7e9d, 0x7e1d,
	0x7d8a, 0x7ce3, 0x7c29, 0x7b5d, 0x7a7d, 0x798a, 0x7884, 0x776c,
	0x7641, 0x7504, 0x73b5, 0x7255, 0x70e2, 0x6f5f, 0x6dca, 0x6c24,
	0x6a6d, 0x68a6, 0x66cf, 0x64e8, 0x62f2, 0x60ec, 0x5ed7, 0x5cb4,
	0x5a82, 0x5842, 0x55f5, 0x539b, 0x5133, 0x4ebf, 0x4c3f, 0x49b4,
	0x471c, 0x447a, 0x41ce, 0x3f17, 0x3c56, 0x398c, 0x36ba, 0x33de,
	0x30fb, 0x2e11, 0x2b1f, 0x2826, 0x2528, 0x2223, 0x1f19, 0x1c0b,
	0x18f8, 0x15e2, 0x12c8, 0x0fab, 0x0c8b, 0x096a, 0x0647, 0x0324,
	0x0000, 0xfcdc, 0xf9b9, 0xf696, 0xf375, 0xf055, 0xed38, 0xea1e,
	0xe708, 0xe3f5, 0xe0e7, 0xdddd, 0xdad8, 0xd7da, 0xd4e1, 0xd1ef,
	0xcf05, 0xcc22, 0xc946, 0xc674, 0xc3aa, 0xc0e9, 0xbe32, 0xbb86,
	0xb8e4, 0xb64c, 0xb3c1, 0xb141, 0xaecd, 0xac65, 0xaa0b, 0xa7be,
	0xa57e, 0xa34c, 0xa129, 0x9f14, 0x9d0e, 0x9b18, 0x9931, 0x975a,
	0x9593, 0x93dc, 0x9236, 0x90a1, 0x8f1e, 0x8dab, 0x8c4b, 0x8afc,
	0x89bf, 0x8894, 0x877c, 0x8676, 0x8583, 0x84a3, 0x83d7, 0x831d,
	0x8276, 0x81e3, 0x8163, 0x80f7, 0x809e, 0x8059, 0x8028, 0x800a,
	0x8000, 0x800a, 0x8028, 0x8059, 0x809e, 0x80f7, 0x8163, 0x81e3,
	0x8276, 0x831d, 0x83d7, 0x84a3, 0x8583, 0x8676, 0x877c, 0x8894,
	0x89bf, 0x8afc, 0x8c4b, 0x8dab, 0x8f1e, 0x90a1, 0x9236, 0x93dc,
	0x9593, 0x975a, 0x9931, 0x9b18, 0x9d0e, 0x9f14, 0xa129, 0xa34c,
	0xa57e, 0xa7be, 0xaa0b, 0xac65, 0xaecd, 0xb141, 0xb3c1, 0xb64c,
	0xb8e4, 0xbb86, 0xbe32, 0xc0e9, 0xc3aa, 0xc674, 0xc946, 0xcc22,
	0xcf05, 0xd1ef, 0xd4e1, 0xd7da, 0xdad8, 0xdddd, 0xe0e7, 0xe3f5,
	0xe708, 0xea1e, 0xed38, 0xf055, 0xf375, 0xf696, 0xf9b9, 0xfcdc
};

static uint8_t step;  // step in sine[]
static uint32_t err;  // accumulator for 1-bit DAC error

/* Synth interrupt handler */
void synthStep(void)
{
	step += wave_divider;
	// calculate DAC error, incrementally improving accuracy
	err = (uint16_t) err - (1<<15) - sine[step];
	// set sounder pin if amplitude exceeds err else clear sounder pin
	LPC_GPIO_PORT->W0[SOUNDER_PIN] = err >> 16;
}

static uint8_t  led_on      = 0;
static uint8_t  rest_ticks  = 0;
static uint8_t  note_ticks  = 0;
static uint16_t event_index = 0;
static uint8_t  play_count  = 0;

#define MAX_PLAY_COUNT 2

/* Music score interrupt handler */
void scoreStep(void)
{
	uint16_t   freq;
	uint16_t*  len;
	NoteEvent* ev;

	if (note_ticks) {
		--note_ticks;
		return;
	}

	if (led_on) {
		// light is off as soon as the note stops sounding
		LPC_GPIO_PORT->W0[LED_PIN] = 0;
		led_on = 0;
	}

	if (rest_ticks) {
		// disable the interrupt
		LPC_MRT->Channel[0].INTVAL = 0;
		LPC_MRT->Channel[0].CTRL = LPC_MRT->Channel[0].CTRL & ~MRT_INT_ENA;
		LPC_GPIO_PORT->W0[SOUNDER_PIN] = 0;
		err = 0;

		--rest_ticks;
		return;
	}

	if (track0[event_index].length == endOfNotes) {
		event_index = 0;
		play_count++;
		if (play_count >= MAX_PLAY_COUNT)
			PMU_DeepPowerDown();
		return;
	}

	ev = &track0[event_index];

	len = note_lengths[ev->length];
	note_ticks = len[0];
	rest_ticks = len[1];

	if (ev->pitch == REST) {
		rest_ticks += note_ticks;
		note_ticks = 0;
		freq = 0;
	}
	else {
		// decompose the flash command and the pitch
		if (ev->pitch & FLASH) {
			LPC_GPIO_PORT->W0[LED_PIN] = 1;
			led_on = 1;
		}
		freq = note_freqs[ev->pitch & ~FLASH];
	}

	if (!freq) {
		// disable the interrupt
		LPC_MRT->Channel[0].INTVAL = 0;
		LPC_MRT->Channel[0].CTRL = LPC_MRT->Channel[0].CTRL & ~MRT_INT_ENA;
		LPC_GPIO_PORT->W0[SOUNDER_PIN] = 0;
		err = 0;
	}
	else {
		// calculate wave sample period taking care to keep this period long
		// enough for agreable pitch production
		if (freq >> 11) {
			wave_divider = 8;
			LPC_MRT->Channel[0].INTVAL = __SYSTEM_CLOCK /  32 / freq;
		}
		else if (freq >> 10) {
			wave_divider = 4;
			LPC_MRT->Channel[0].INTVAL = __SYSTEM_CLOCK /  64 / freq;
		}
		else if (freq >> 9) {
			wave_divider = 2;
			LPC_MRT->Channel[0].INTVAL = __SYSTEM_CLOCK / 128 / freq;
		}
		else {
			wave_divider = 1;
			LPC_MRT->Channel[0].INTVAL = __SYSTEM_CLOCK / 256 / freq;
		}
		// force loading the timer value
		LPC_MRT->Channel[0].INTVAL |= 0x1UL<<31;
		// enable the timer interrupt
		LPC_MRT->Channel[0].CTRL = MRT_REPEATED_MODE | MRT_INT_ENA;
	}

	++event_index;
}

void MRT_IRQHandler(void)
{
	if (LPC_MRT->Channel[0].STAT & MRT_STAT_IRQ_FLAG) {
		/* clear interrupt flag */
		LPC_MRT->Channel[0].STAT = MRT_STAT_IRQ_FLAG;
		synthStep();
	}

	if (LPC_MRT->Channel[1].STAT & MRT_STAT_IRQ_FLAG) {
		/* clear interrupt flag */
		LPC_MRT->Channel[1].STAT = MRT_STAT_IRQ_FLAG;
		scoreStep();
	}
}

void main(void)
{
	uint32_t delays[4] =
		{
			__SYSTEM_CLOCK,  // set up once per second ticks
			__SYSTEM_CLOCK * 60 * 4 / WHOLE / TEMPO,
			0,
			0
		};

	/* Populate the note frequency array */
	compute_note_freqs();

	/* Initialise the GPIO block */
	gpioInit();

	/* Enable all 6 GPIO. */
	LPC_SWM->PINENABLE0 = 0xffffffffUL;

	/* Configure the multi-rate timer for synth and note ticks */
	mrtInit(delays);

	gpioSetDir(SOUNDER_PIN, OUTPUT);
	gpioSetDir(LED_PIN, OUTPUT);

	while (1)
		// wait for interrupt
		__WFI();
}

void PMU_DeepPowerDown(void)
{
	uint32_t regVal;

	if ((LPC_PMU->PCON & (0x1<<11)) != 0x0) {
		/* Check deep power down bits. If deep power down mode is
		   entered, clear the PCON bits. */
		regVal = LPC_PMU->PCON;
		regVal |= (0x1<<11);
		LPC_PMU->PCON = regVal;

		if (LPC_PMU->GPREG0 != 0x12345678 ||
		    LPC_PMU->GPREG1 != 0x87654321 ||
		    LPC_PMU->GPREG2 != 0x56781234 ||
		    LPC_PMU->GPREG3 != 0x43218765)
			while (1);
	}
	else {
		/* If in neither sleep nor deep power mode, enter deep power
		   down mode now. */
		LPC_PMU->GPREG0 = 0x12345678;
		LPC_PMU->GPREG1 = 0x87654321;
		LPC_PMU->GPREG2 = 0x56781234;
		LPC_PMU->GPREG3 = 0x43218765;
		SCB->SCR |= 0x4;
#if __WKT
		LPC_PMU->DPDCTRL |= (0x3 << 2);
		init_wkt(1, 10000 * 10);
#endif
		LPC_PMU->PCON = 0x3;
		__WFI();
	}
	return;
}
