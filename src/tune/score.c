/*
 * Copyright (c) 2014 Vladimir Komendantskiy
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LPC8xx.h"
#include "score.h"

uint16_t note_lengths[endOfNotes][2] = {
	/*  play time -- rest time   */
	{WHOLE,          0},
	{HALF,           0},
	{_4TH_LEGATO,    _4TH_LEGATO_REST},
	{_4TH,           _4TH_REST},
//	{_4TH_STACCATO,  _4TH_STACCATO_REST},
	{_8TH_LEGATO,    _8TH_LEGATO_REST},
	{_8TH,           _8TH_REST},
	{_8TH_STACCATO,  _8TH_STACCATO_REST},
	{_16TH_LEGATO,   _16TH_LEGATO_REST},
	{_16TH,          _16TH_REST},
	{_16TH_STACCATO, _16TH_STACCATO_REST},
	{_32TH_LEGATO,   _32TH_LEGATO_REST},
//	{_32TH,          _32TH_REST},
//	{_32TH_STACCATO, _32TH_STACCATO_REST},
};

/*
 * П. И. Чайковский: Танец феи Драже.
 */
NoteEvent track0[] = {
	/*
	// 1
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 6),
	},
	// 2
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 3),
	},
	// 3
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 6),
	},
	// 4
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 3),
	},
	// 5
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 6),
	},
	// 6
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 8),
	},
	// 7
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 6),
	},
	// 8
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 2),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 0),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(2, 11),
	},
	// 9
	{
		.length = _8th,
		.pitch  = NOTE(2, 10),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 0),
	},
	{
		.length = _8th,
		.pitch  = NOTE(2, 9),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 0),
	},
	// 10
	{
		.length = _8th,
		.pitch  = NOTE(2, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _8th,
		.pitch  = NOTE(2, 6),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	// 11
	{
		.length = _8th_staccato,
		.pitch  = NOTE(2, 6),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 2),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(2, 6),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 6),
	},
	// 12
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 6),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 6),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 7),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 6),
	},
	// 13
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 6),
	},
	// 14
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 8),
	},
	// 15
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 6),
	},
	// 16
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 6),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 2),
	},
	// 17
	{
		.length = _4th_legato,
		.pitch  = NOTE(4, 1),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 1),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 6),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 3),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 1),
	},
	// 18
	{
		.length = _4th_legato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 2),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 1),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 11),
	},
	// 19
	{
		.length = _4th_legato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 2),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 0),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 9),
	},
	// 20
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 3),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 9),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _8th,
		.pitch  = NOTE(3, 11),
	},
	*/
	// 28
	{
		.length = half,
		.pitch  = REST,
	},
	// 29
	{
		.length = half,
		.pitch  = REST,
	},
	// 30
	{
		.length = half,
		.pitch  = REST,
	},
	// 31
	{
		.length = half,
		.pitch  = REST,
	},
	// 32
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 6) | FLASH,
	},
	// 33
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 3),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	// 34
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	// 35
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	// 36
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 7) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 7) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	// 37
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 0),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	// 38
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 6) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	// 39
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 3) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 6),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 3) | FLASH,
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	// 40
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 6) | FLASH,
	},
	// 41
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 3),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	// 42
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	// 43
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	// 44
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 1),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 3) | FLASH,
	},
	// 45
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 2) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	// 46
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 9),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 0) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 11) | FLASH,
	},
	// 47
	{
		.length = _8th,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(6, 3),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(6, 6),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(6, 11),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(6, 11),
	},
	// 48
	{
		.length = _16th,
		.pitch  = NOTE(5, 1),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 3) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 6) | FLASH,
	},
	// 49
	{
		.length = _16th,
		.pitch  = NOTE(5, 7),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 3) | FLASH,
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	// 50
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 3),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_legato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _16th,
		.pitch  = REST,
	},
	// 51
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 1),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 2),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 11),
	},
	// 52
	{
		.length = _16th,
		.pitch  = NOTE(5, 1),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 1) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 3) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 6) | FLASH,
	},
	// 53
	{
		.length = _16th,
		.pitch  = NOTE(5, 7),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 7) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 4) | FLASH,
	},
	{
		.length = _16th,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _16th,
		.pitch  = NOTE(6, 3) | FLASH,
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	// 54
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 3),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_legato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _16th,
		.pitch  = REST,
	},
	// 55
	{
		.length = _4th_legato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _8th,
		.pitch  = NOTE(3, 11),
	},
	// 56
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 1),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 3),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 11),
	},
	// 57
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 3),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 6),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(3, 11),
	},
	// 58
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 6),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _16th_legato,
		.pitch  = NOTE(3, 7),
	},
	// 59
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 9),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 11),
	},
	{
		.length = _16th_legato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _8th_legato,
		.pitch  = NOTE(4, 11),
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	// 60
	{
		.length = half,
		.pitch  = REST,
	},
	// 61
	{
		.length = half,
		.pitch  = REST,
	},
	// 62
	{
		.length = half,
		.pitch  = REST,
	},
	// 63
	{
		.length = _4th,
		.pitch  = REST,
	},
	{
		.length = _8th,
		.pitch  = REST,
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 9),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 6),
	},
	// 64
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6),
	},
	// 65
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 7) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 7),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 8) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 8),
	},
	// 66
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6),
	},
	// 67
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 2),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 0),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(3, 11),
	},
	// 68
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 10),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 10),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 0) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 0),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 9),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 0) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 0),
	},
	// 69
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 7),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 7),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	// 70
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(3, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6),
	},
	// 71
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 9),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 6),
	},
	// 72
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6),
	},
	// 73
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 7) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 7),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 8) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 8),
	},
	// 74
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(4, 4),
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6) | FLASH,
	},
	{
		.length = _16th_staccato,
		.pitch  = NOTE(5, 6),
	},
	// 75
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 7),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 2),
	},
	// 76
	{
		.length = _4th_legato,
		.pitch  = NOTE(5, 1) | FLASH,
	},
	{
		.length = _4th_legato,
		.pitch  = NOTE(5, 1),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 6),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 1),
	},
	// 77
	{
		.length = _4th_legato,
		.pitch  = NOTE(4, 11) | FLASH,
	},
	{
		.length = _4th_legato,
		.pitch  = NOTE(4, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 4),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 2),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 1),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 11),
	},
	// 78
	{
		.length = _4th_legato,
		.pitch  = NOTE(4, 9) | FLASH,
	},
	{
		.length = _4th_legato,
		.pitch  = NOTE(4, 9),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 2),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(5, 0),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 11),
	},
	{
		.length = _32th_legato,
		.pitch  = NOTE(4, 9),
	},
	// 79
	{
		.length = _16th_legato,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _16th_legato,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _16th_legato,
		.pitch  = NOTE(5, 3),
	},
	{
		.length = _16th,
		.pitch  = REST,
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 7),
	},
	{
		.length = _8th_staccato,
		.pitch  = NOTE(4, 11) | FLASH,
	},
	// end of track
	{
		.length = endOfNotes,
		.pitch  = REST,
	}
};
