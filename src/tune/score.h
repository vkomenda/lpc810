/*
 * Copyright (c) 2014 Vladimir Komendantskiy
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SCORE_H
#define __SCORE_H

#define TEMPO 40 // andante non tropo, 2/4

// note lengths
#define WHOLE               192    // 64th triplet max. resolution
//#define WHOLE 1024

#define HALF                (WHOLE / 2)

#define _4TH_LEGATO         (WHOLE / 4)
#define _4TH_LEGATO_REST    0
#define _4TH                (WHOLE / 4 - WHOLE / 4 / 4)
#define _4TH_REST           (_4TH_LEGATO - _4TH)
#define _4TH_STACCATO       (WHOLE / 4 - WHOLE / 4 / 2)
#define _4TH_STACCATO_REST  (_4TH_LEGATO - _4TH_STACCATO)

#define _8TH_LEGATO         (WHOLE / 8)
#define _8TH_LEGATO_REST    0
#define _8TH                (WHOLE / 8 - WHOLE / 8 / 4)
#define _8TH_REST           (_8TH_LEGATO - _8TH)
#define _8TH_STACCATO       (WHOLE / 8 - WHOLE / 8 / 2)
#define _8TH_STACCATO_REST  (_8TH_LEGATO - _8TH_STACCATO)

#define _16TH_LEGATO        (WHOLE / 16)
#define _16TH_LEGATO_REST   0
#define _16TH               (WHOLE / 16 - WHOLE / 16 / 4)
#define _16TH_REST          (_16TH_LEGATO - _16TH_STACCATO)
#define _16TH_STACCATO      (WHOLE / 16 - WHOLE / 16 / 2)
#define _16TH_STACCATO_REST (_16TH_LEGATO - _16TH_STACCATO)

#define _32TH_LEGATO        (WHOLE / 32)
#define _32TH_LEGATO_REST   0
#define _32TH               (WHOLE / 32 - WHOLE / 32 / 4)
#define _32TH_REST          (_32TH_LEGATO - _32TH_STACCATO)
#define _32TH_STACCATO      (WHOLE / 32 - WHOLE / 32 / 2)
#define _32TH_STACCATO_REST (_32TH_LEGATO - _32TH_STACCATO)

typedef enum NoteLength {
	whole,
	half,
	_4th_legato,
	_4th,
//	_4th_staccato,
	_8th_legato,
	_8th,
	_8th_staccato,
	_16th_legato,
	_16th,
	_16th_staccato,
	_32th_legato,
//	_32th,
//	_32th_staccato,
	endOfNotes,
} NoteLength;

uint16_t note_lengths[endOfNotes][2];

#define REST  0xff
#define FLASH 0x80

typedef struct NoteEvent {
	NoteLength length; // note length relative to the measure length
	uint8_t    pitch;  // note pitch 0.. were 0xff denotes a rest
} NoteEvent;

#define BASE_FREQ 98   // approx. frequency of G2
#define NOTE(octave, note) (12 * (octave - 2) + note - 7) // index in note_freqs[]

extern NoteEvent track0[]; // __attribute__ ((aligned (8)));

//uint32_t note_freqs[72];
//void compute_note_freqs(void);

//NoteEvent treble[];
//NoteEvent bass[];

#endif // __SCORE_H
