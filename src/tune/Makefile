CROSS = arm-none-eabi-
CPU = -mthumb -mcpu=cortex-m0plus -MD
FPU = # -mfloat-abi=soft

FILENAME = tune
LINKERSCRIPT = lpc810.ld

CFLAGS+= -Os --specs=nano.specs -ffunction-sections -fdata-sections -fno-builtin

CC = ${CROSS}gcc
LD = ${CROSS}ld
OBJCOPY = ${CROSS}objcopy
SIZE = ${CROSS}size

# use these on the $(LD) line, as needed:
LIBM = ${shell ${CC} ${CFLAGS} --print-file-name=libm.a}
LIBC = ${shell ${CC} ${CFLAGS} --print-file-name=libc.a}
LIBGCC = ${shell ${CC} ${CFLAGS} --print-libgcc-file-name}

CFLAGS += $(CPU) $(FPU)
LDFLAGS += --gc-sections

LIBDIR = ../../lib
CMSISDIR = ../../cmsis
DRIVERDIR = ../../lpc800_driver_lib
INC += -I$(CMSISDIR) -I$(LIBDIR) # -I$(DRIVERDIR)/inc
CFLAGS += $(INC) -D__USE_CMSIS   # -DCLOCK_SETUP=1

TUNE_OBJS = $(FILENAME).o \
	score.o \
        $(CMSISDIR)/system_LPC8xx.o \
        $(LIBDIR)/gpio.o \
        $(LIBDIR)/mrt.o \
        $(LIBDIR)/gcc_startup_lpc8xx.o

all: $(FILENAME).bin

%.axf: $(LINKERSCRIPT) $(TUNE_OBJS)
	@$(LD) -o $@ $(LDFLAGS) -T $(LINKERSCRIPT) $(TUNE_OBJS) $(LIBGCC)
	-@echo ""
	@$(SIZE) $(FILENAME).axf

clean:
	@rm -f *.o *.d
	@rm -f $(LIBDIR)/*.o $(LIBDIR)/*.d
	@rm -f $(FILENAME).axf $(FILENAME).hex $(FILENAME).bin

%.bin:%.axf
	-@echo ""
	-@echo "Generating $(FILENAME).hex"
	@$(OBJCOPY) --strip-unneeded -O ihex $(FILENAME).axf $(FILENAME).hex
	-@echo "Generating $(FILENAME).bin"
	@$(OBJCOPY) --strip-unneeded -O binary $(FILENAME).axf $(FILENAME).bin
