#ifndef __NOTES_H
#define __NOTES_H

// Key frequences in milliHertz
#define FREQ_C3  130813
#define FREQ_Db3 138591
#define FREQ_D3  146832
#define FREQ_Eb3 155563
#define FREQ_E3  164814
#define FREQ_F3  174614
#define FREQ_Gb3 184997
#define FREQ_G3  195998
#define FREQ_Ab3 207652
#define FREQ_A3  220000
#define FREQ_Bb3 233082
#define FREQ_B3  246942
#define FREQ_C4  261626
#define FREQ_Db4 277183
#define FREQ_D4  293665
#define FREQ_Eb4 311127
#define FREQ_E4  329628
#define FREQ_F4  349228
#define FREQ_Gb4 369994
#define FREQ_G4  391995
#define FREQ_Ab4 415305
#define FREQ_A4  440000
#define FREQ_Bb4 466164
#define FREQ_B4  493883
#define FREQ_C5  523251

// 100% duty cycle in microseconds derived from milliHertz
#define us_of_mHz(mHz) (1000000000 / mHz)

typedef struct note {
	unsigned short int on;
	unsigned short int off;
	unsigned       int dur;
} note;

// Tempo is expressed in the microsecond duration of 1/4th (one beat period).
#define TEMPO_ADAGIO    1000000
#define TEMPO_ANDANTE   714286
#define TEMPO_MODERATO  625000
#define TEMPO_ALLEGRO   500000
#define TEMPO_VIVACE    454545
#define TEMPO_PRESTO    357143

// Example: 3/16th G in adagio tempo is NOTE(FREQ_G, 3, 16, TEMPO_ADAGIO)
#define NOTE(freq, numer, denom, tempo)		\
	{					\
		.on  = us_of_mHz(freq),		\
		.off = us_of_mHz(freq),		\
		.dur = tempo * numer / denom,	\
	}

#define REST(numer, denom, tempo)		\
	{					\
		.on  = 0;			\
		.off = tempo * numer / denom;	\
		.dur = tempo * numer / denom;	\
	}

#endif // __NOTES_H
